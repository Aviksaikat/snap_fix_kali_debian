#!/bin/bash

work (){
   #echo $1
   sudo snap disable $1 2>/dev/null;
   sudo snap enable $1 2>/dev/null;
}

call (){
   programs=("$@")
   #echo $programs
   
   for i in "${programs[@]}"
   do
      # Parallel processing 
      work $i &
   done
}


programs=()
# name of the snap packages
#programs=(atom audacity obsidian signal-desktop bitwarden brave breaktimer chromium code code-insiders deluge-lukewh evince libreoffice notion-snap  postman powershell qbittorrent-arnatious remmina snap-store spotify telegram-desktop todoist)

if [ -z "${programs}" ];
then 
   $(which snap) list | cut -d ' ' -f1 | tail -n +2 | while read line; 
   do
      work $line &
   done
   
else
   call "${programs[@]}"
fi
