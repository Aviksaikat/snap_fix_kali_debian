# Snap fix for kali(debian based distros)

> Saikat Karmakar | Sept : 2021
---
***Run as admin otherwise the script will not work***
## Usage:
```bash
git clone https://gitlab.com/Aviksaikat/snap_fix_kali_debian.git
cd snap_fix_kali_debian
chmod +x snap-fix.sh
sudo ./snap-fix.sh
```


- This is not a permanent fix but this does work. You have to run this everytime your system reboots. I'll suggest add this script to the startup scripts list.
- This script may try to disable the snap core as well but snap automatically stops it(on my machine). It is recommended to list your snap packages using this command `$(which snap) list | cut -d ' ' -f1 | tail -n +2 | tr '\n' ' '` and store the output to the `programs` array.